using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using static TextureUtils;

public class cc0texturesDotComConverter : TextureTool
{
    static readonly string EDITOR_PREFS_PREFIX = "CC0TEXTURES_CONVERTER_";

    Texture2D SampleTexture;

    float DefaultAO_Value = 1.0f;
    float DefaultColValue = 0.5f;
    float DefaultDisValue = 1.0f;
    float DefaultMetValue = 0.0f;
    float DefaultSmoValue = 1.0f;


    float DefaultDetValue = 1.0f;


    bool GenerateMaskMap = true;
    bool GenerateDetailMap = false;
    bool GenerateCorrectedNormals = true;
    bool GenerateMaterial = true;
    bool GenerateTerrainLayer = false;
    int AnisoLevel = 0;
    bool DeleteLeftovers = true;

    Vector2Int OutputFileResolution = new Vector2Int(4096, 4096);
    

    private readonly string[] TextureTypes =
    {
        "AmbientOcclusion", 
        "Color", 
        "Displacement", 
        "Metalnes", 
        "Normal", 
        "Roughness", 
    };

    public cc0texturesDotComConverter()
    {

    }
    

    public string GetName()
    {
        return "cc0textures.com Converter";
    }
    
    public string GetEditorPrefsPrefix()
    {
        return EDITOR_PREFS_PREFIX;
    }
    
    public void ExternOnGUI()
    {
        GUILayout.Label("Any texture (for directory detection)", EditorStyles.boldLabel);

        //Albedo Map Input
        SampleTexture = (Texture2D) EditorGUILayout.ObjectField("Texture from target directory", SampleTexture, typeof (Texture2D), false); 
        //Albedo Default
        GUI.enabled = false;
        if (SampleTexture == null)
            GUILayout.Label("No directory selected");
        else
            GUILayout.Label("Grabbing textures from " + TextureUtils.FullNameToUnityName(GetDirectoryFromFirstNonNullTexture(SampleTexture).FullName),  EditorStyles.wordWrappedLabel);
        GUI.enabled = true;

        //Output options
        //Enforce prerequisites
        if (GenerateMaterial || GenerateTerrainLayer)
        {
            GenerateMaskMap = true;
            GenerateCorrectedNormals = true;
        }
        EditorGUILayout.Space();
        GUILayout.Label("Output Options", EditorStyles.boldLabel);
        //Checkboxes for output preferences
        GenerateMaskMap          = EditorGUILayout.Toggle(   "Mask Map"             , GenerateMaskMap         , GUILayout.ExpandWidth(false));
        GenerateDetailMap        = EditorGUILayout.Toggle(   "Detail Map"           , GenerateDetailMap       , GUILayout.ExpandWidth(false));
        GenerateCorrectedNormals = EditorGUILayout.Toggle(   "Correct Normals"      , GenerateCorrectedNormals, GUILayout.ExpandWidth(false));
        GenerateMaterial         = EditorGUILayout.Toggle(   "Material"             , GenerateMaterial        , GUILayout.ExpandWidth(false));
        GenerateTerrainLayer     = EditorGUILayout.Toggle(   "Terrain Layer"        , GenerateTerrainLayer    , GUILayout.ExpandWidth(false));
        AnisoLevel               = EditorGUILayout.IntSlider(new GUIContent(
            "Aniso Level",
            "Aniso Level 8 is HIGHLY recommended if these textures are being used for terrain. Level 4 or higher is recommended for floors or walls. Anything that's large and expected to be viewed from a steep angle should use some amount above 0. " + 
            "Using Anisotropic Filtering takes additional GPU memory so you shouldn't use it on textures that don't need it. You can change this later in the Inspector with the textures selected"),
            AnisoLevel, 0, 16
        );
        if (GenerateTerrainLayer)
        {
            GUI.enabled = false;
            GUILayout.Label("Aniso Level 8 is HIGHLY recommended if these textures are being used for terrain.", EditorStyles.wordWrappedLabel);
            GUI.enabled = true;
        }
        else
        {
            GUI.enabled = false;
            GUILayout.Label("Use an Aniso Level above 0 if these textures are intended to be viewed from a steep angle. Hover over Aniso Level for details",  EditorStyles.wordWrappedLabel);
            GUI.enabled = true;
        }
        DeleteLeftovers      = EditorGUILayout.Toggle("Delete leftover files", DeleteLeftovers         , GUILayout.ExpandWidth(false));


        //Begin horizontal resolution picker
        GUILayout.BeginHorizontal();
        GUILayout.Label("Output Resolution");
        OutputFileResolution = EditorGUILayout.Vector2IntField("", OutputFileResolution);
        EditorGUILayout.EndHorizontal();

        if(GUILayout.Button(new GUIContent("Auto-set resoltion", "Reads the original texture asset from the disk and sets the output resolution equal to the selected texture")))
        {
            if (SampleTexture == null)
            {
                Debug.LogWarning("Select the directory before attempting to auto-set resolution");
                goto CANCEL_AUTORES;
            }

            OutputFileResolution = AutoResolution(SampleTexture);
        }
        CANCEL_AUTORES:



        if (GUILayout.Button(new GUIContent("Generate", "Generate selected maps (WARNING: WILL DELETE FILES IF \"Delete Leftovers\" is on")))
        {
            if (SampleTexture == null)
            {
                EditorUtility.DisplayDialog("Error!", "Please place a texture in the \"Sample Texture\" slot, this texture's directory will be searched for the various texture types. Make sure you don't rename the extracted textures either!", "Okay");
            }
            else
            {
                Render();
            }
        }
    }
    private void Render()
    {
        long renderStartTime = System.DateTime.Now.Ticks;

        //---------------------------Initialize Input Textures---------------------------

        //Get directory
        DirectoryInfo textureDir = GetDirectoryFromFirstNonNullTexture(SampleTexture);

        //Get name for assets
        string[] tmpNameArray = SampleTexture.name.Split('_');
        string namePrefix = tmpNameArray[tmpNameArray.Length - 3];

        //Detect all textures
        Texture2D AO_ = TextureUtils.AutoDetectTexture(textureDir, new string[] {"_" + TextureTypes[0]});
        Texture2D Col = TextureUtils.AutoDetectTexture(textureDir, new string[] {"_" + TextureTypes[1]});
        Texture2D Dis = TextureUtils.AutoDetectTexture(textureDir, new string[] {"_" + TextureTypes[2]});
        Texture2D Met = TextureUtils.AutoDetectTexture(textureDir, new string[] {"_" + TextureTypes[3]});
        Texture2D Nor = TextureUtils.AutoDetectTexture(textureDir, new string[] {"_" + TextureTypes[4]});
        Texture2D Smo = TextureUtils.AutoDetectTexture(textureDir, new string[] {"_" + TextureTypes[5]});

        //Generate data for progress bars
        int texturesIn = 0;
        int texturesOut = 0;
        //--In
        if (AO_ != null) texturesIn++;
        if (Col != null) texturesIn++;
        if (Dis != null) texturesIn++;
        if (Met != null) texturesIn++;
        if (Nor != null) texturesIn++;
        if (Smo != null) texturesIn++;
        //--Out
        if (GenerateMaskMap)            texturesOut++;
        if (GenerateDetailMap)          texturesOut++;
        if (GenerateTerrainLayer)       texturesOut++;
        if (GenerateCorrectedNormals)   texturesOut++;

        

        //Debug prints
        if (AO_ == null) Debug.Log(AO_ == null ? "No suitable AmbientOcclusion texture found" : "Found AmbientOcclusion Texture: " + AO_.name);
        else                  Debug.Log("Using AmbientOcclusion: " + AO_.name);

        if (Col == null) Debug.Log(Col == null ? "No suitable Color texture found" : "Found Color Texture: " + Col.name);
        else                  Debug.Log("Using Color: " + Col.name);

        if (Dis == null) Debug.Log(Dis == null ? "No suitable Displacement texture found" : "Found Displacement Texture: " + Dis.name);
        else                  Debug.Log("Using Displacement: " + Dis.name);

        if (Met == null) Debug.Log(Met == null ? "No suitable Met texture found" : "Found Met Texture: " + Met.name);
        else                  Debug.Log("Using Met: " + Met.name);

        if (Nor == null) Debug.Log(Nor == null ? "No suitable Nor texture found" : "Found Nor Texture: " + Nor.name);
        else                  Debug.Log("Using Nor: " + Nor.name);

        if (Smo == null) Debug.Log(Smo == null ? "No suitable Smo texture found" : "Found Smo Texture: " + Smo.name);
        else                  Debug.Log("Using Smo: " + Smo.name);

        //Texture to render to
        Texture2D MaskMapTexture = new Texture2D(OutputFileResolution.x, OutputFileResolution.y);
        Texture2D DetailMTexture = new Texture2D(OutputFileResolution.x, OutputFileResolution.y);
        Texture2D TerrainTexture = new Texture2D(OutputFileResolution.x, OutputFileResolution.y);
        Texture2D NormalsTexture = new Texture2D(OutputFileResolution.x, OutputFileResolution.y);

        //Easy access to OutputTexture data
        int width  = OutputFileResolution.x;
        int height = OutputFileResolution.y;
        int length = width * height;

        //Get pixel data straight from the disk or fake it
        EditorUtility.DisplayProgressBar("Rendering...", "Reading and preparing textures", 0f / 6f);
        Color[] AO_Pixels = TextureUtils.GetPixelDataFromDisk(OutputFileResolution, DefaultAO_Value, AO_);
        EditorUtility.DisplayProgressBar("Rendering...", "Reading and preparing textures", 1f / 6f);
        Color[] ColPixels = TextureUtils.GetPixelDataFromDisk(OutputFileResolution, DefaultColValue, Col);
        EditorUtility.DisplayProgressBar("Rendering...", "Reading and preparing textures", 2f / 6f);
        Color[] DisPixels = TextureUtils.GetPixelDataFromDisk(OutputFileResolution, DefaultDisValue, Dis);
        EditorUtility.DisplayProgressBar("Rendering...", "Reading and preparing textures", 3f / 6f);
        Color[] MetPixels = TextureUtils.GetPixelDataFromDisk(OutputFileResolution, DefaultMetValue, Met);
        EditorUtility.DisplayProgressBar("Rendering...", "Reading and preparing textures", 4f / 6f);
        Color[] SmoPixels = TextureUtils.GetPixelDataFromDisk(OutputFileResolution, DefaultSmoValue, Smo);

        //Normal is special and cannot default to a single value, Load normall if not null, otherwise fake a standard Normal map
        EditorUtility.DisplayProgressBar("Rendering...", "Reading and preparing textures", 5f / 6f);
        Color[] NorPixels;
        
        if (Nor != null)
        {   //Load normally
            NorPixels = TextureUtils.GetPixelDataFromDisk(OutputFileResolution, 0.5f, Nor);
        }
        else
        {   //Generate a default normal
            NorPixels = new Color[length];
            Color defaultNormalColor = new Color(0.5f, 0.5f, 1.0f);

            for (int i = 0; i < length; i++)
            {
                NorPixels[i] = defaultNormalColor;
            }
        }


        //---------------------------Begin Rendering---------------------------
        //Output pixels
        Color[] MaskMapOutPixels = new Color[length];
        Color[] DetailMOutPixels = new Color[length];
        Color[] TerrainOutPixels = new Color[length];
        Color[] NormalsOutPixels = new Color[length];

        //Used so the progress bar only updates 100 times
        int lengthDiv100 = length / 100;


        if (GenerateMaskMap)
        {
            //Render MaskMap
            for (int i = 0; i < length; i++)
            {
                //Metal on R channel
                MaskMapOutPixels[i].r = MetPixels[i].r;
                //Occlusion on G channel
                MaskMapOutPixels[i].g = AO_Pixels[i].r;
                //DetailMask on B channel
                MaskMapOutPixels[i].b = DefaultDetValue;
                //Smoothness on A channel
                MaskMapOutPixels[i].a = 1f - SmoPixels[i].r;

                //Output every 256 pixel updates
                if (i % lengthDiv100 == 0)
                    EditorUtility.DisplayProgressBar("Rendering...", "Rendering MaskMap (" + i + "/" + length + ")", (float) i / (float) length);
            }
        }

        if (GenerateDetailMap)
        {
            //Render DetailMap
            for (int i = 0; i < length; i++)
            {
                //Use Albedo Grayscale for Red
                DetailMOutPixels[i].r = TextureUtils.ToGrayscaleFloat(ColPixels[i]);
                //Use Normal Green/Y for Green
                DetailMOutPixels[i].g = NorPixels[i].g;
                //Use Smoothness Alpha-or-Red for Blue
                DetailMOutPixels[i].b = 1f - SmoPixels[i].r;  //Use Red   from Smoothness
                //Use Normal Red/X for Alpha
                DetailMOutPixels[i].a = NorPixels[i].r;

                //Output 100 times
                if (i % lengthDiv100 == 0)
                    EditorUtility.DisplayProgressBar("Rendering...", "Rendering DetailMap (" + i + "/" + length + ")", (float) i / (float) length);
            }
        }

        if (GenerateCorrectedNormals)
        {
            //Render MaskMap
            for (int i = 0; i < length; i++)
            {
                //Metal on R channel
                NormalsOutPixels[i].r = NorPixels[i].r;
                //Occlusion on G channel
                NormalsOutPixels[i].g = 1f - NorPixels[i].g;
                //DetailMask on B channel
                NormalsOutPixels[i].b = NorPixels[i].b;
                //Smoothness on A channel
                NormalsOutPixels[i].a = NorPixels[i].a;

                //Output every 256 pixel updates
                if (i % lengthDiv100 == 0)
                    EditorUtility.DisplayProgressBar("Rendering...", "Rendering Corrected Normal (" + i + "/" + length + ")", (float) i / (float) length);
            }
        }

        if (GenerateTerrainLayer)
        {
            //Render MaskMap
            for (int i = 0; i < length; i++)
            {
                //Copy Albedo
                TerrainOutPixels[i].r = ColPixels[i].r;
                //Copy Albedo
                TerrainOutPixels[i].g = ColPixels[i].g;
                //Copy Albedo
                TerrainOutPixels[i].b = ColPixels[i].b;
                //Smoothness on A channel
                TerrainOutPixels[i].a = 1f - SmoPixels[i].r;

                //Output every 256 pixel updates
                if (i % lengthDiv100 == 0)
                    EditorUtility.DisplayProgressBar("Rendering...", "Rendering Terrain Color (" + i + "/" + length + ")", (float) i / (float) length);
            }
        }

        //Debug
        EditorUtility.DisplayProgressBar("Rendering...", "Writing Pixel data", 0f);

        //Update image pixels
        if (GenerateMaskMap)            MaskMapTexture.SetPixels(MaskMapOutPixels);
        if (GenerateDetailMap)          DetailMTexture.SetPixels(DetailMOutPixels);
        if (GenerateTerrainLayer)       TerrainTexture.SetPixels(TerrainOutPixels);
        if (GenerateCorrectedNormals)   NormalsTexture.SetPixels(NormalsOutPixels);
        //Apply the modified pixels to the image
        if (GenerateMaskMap)            MaskMapTexture.Apply();
        if (GenerateDetailMap)          DetailMTexture.Apply();
        if (GenerateTerrainLayer)       TerrainTexture.Apply();
        if (GenerateCorrectedNormals)   NormalsTexture.Apply();


        //--------------------------Disk writes-------------------------------

        //Rename Color and Height
        //Used to decide which slots to assign the MaskMap to. Needs to be done before these assets are deleted.
        bool hasMet = Met != null || Smo != null;
        bool hasAO_ = AO_ != null;


        if (DeleteLeftovers)
        {
            //Delete combined and altered textures
            if (AO_ != null) AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(AO_));
            //if (Col != null) AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(Col)); //Rename later, do not delete
            //if (Dis != null) AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(Dis)); //Rename later, do not delete
            if (Met != null) AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(Met));
            if (Nor != null) AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(Nor));
            if (Smo != null) AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(Smo));

            //Rename color textures
            EditorUtility.DisplayProgressBar("Rendering...", "Renaming Color asset" , 0f / 6f);
            if (Col != null) AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(Col), namePrefix + "_" + "Color");
            EditorUtility.DisplayProgressBar("Rendering...", "Renaming Height asset", 1f / 6f);
            if (Dis != null) AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(Dis), namePrefix + "_" + "Height");

            //Rename maps that Unity can't use at all
            Texture2D IdMask = AutoDetectTexture(textureDir, new string[] {"_IdMask"});
            if (IdMask != null) AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(IdMask));
        }
        //Output additional maps
        EditorUtility.DisplayProgressBar("Rendering...", "Writing and importing MaskMap"        , 2f / 6f);
        if (GenerateMaskMap)            WriteToDisk(textureDir, namePrefix + "_" + "MaskMap"       , MaskMapTexture);
        EditorUtility.DisplayProgressBar("Rendering...", "Writing and importing DetailMap"      , 3f / 6f);
        if (GenerateDetailMap)          WriteToDisk(textureDir, namePrefix + "_" + "DetailMap"     , DetailMTexture);
        EditorUtility.DisplayProgressBar("Rendering...", "Writing and importing Terrain Albedo" , 4f / 6f);
        if (GenerateTerrainLayer)       WriteToDisk(textureDir, namePrefix + "_" + "Color_Terrain", TerrainTexture);
        EditorUtility.DisplayProgressBar("Rendering...", "Writing and importing OpenGL Normal"  , 5f / 6f);
        if (GenerateCorrectedNormals)   WriteToDisk(textureDir, namePrefix + "_" + "Normal_OpenGL" , NormalsTexture, true);

        
        EditorUtility.DisplayProgressBar("Rendering...", "Generating Material", 1f);
        //Use the generated textures in a material
        Material newMaterial = null;
        if (GenerateMaterial)
        {
            //Create a new material
            newMaterial = new Material(Shader.Find("Standard"));

            //Retrieve assets from the Asset manager
            Texture2D tmpMaskMapTexture = TextureUtils.AutoDetectTexture(textureDir, new string[] {"_MaskMap"});
            Texture2D tmpNormalsTexture = TextureUtils.AutoDetectTexture(textureDir, new string[] {"_Normal_OpenGL"});

            //Basic initialization
            newMaterial.SetTexture("_MainTex", Col);
            newMaterial.SetTexture("_MetallicGlossMap", hasMet ? tmpMaskMapTexture : null);
            newMaterial.SetFloat  ("_Glossiness", 1.0f);
            newMaterial.SetTexture("_BumpMap", tmpNormalsTexture);
            newMaterial.SetTexture("_ParallaxMap", Dis);
            newMaterial.SetTexture("_OcclusionMap", hasAO_ ? tmpMaskMapTexture : null);

            AssetDatabase.CreateAsset(newMaterial, TextureUtils.FullNameToUnityName(textureDir.FullName + "\\" + namePrefix + "_Mat.mat"));
            AssetDatabase.ImportAsset(TextureUtils.FullNameToUnityName(textureDir.FullName + "\\" + namePrefix + "_Mat.mat"), ImportAssetOptions.ForceUpdate);
        }

        EditorUtility.DisplayProgressBar("Rendering...", "Generating TerrainLayer", 1f);
        TerrainLayer newLayer = null;
        if (GenerateTerrainLayer)
        {
            //Create a new Terrain Layer
            newLayer = new TerrainLayer();

            //Retrieve assets from the Asset manager
            
            Texture2D tmpTerrainTexture = TextureUtils.AutoDetectTexture(textureDir, new string[] {"_Color_Terrain"});
            Texture2D tmpNormalsTexture = TextureUtils.AutoDetectTexture(textureDir, new string[] {"_Normal_OpenGL"});
            Texture2D tmpMaskMapTexture = TextureUtils.AutoDetectTexture(textureDir, new string[] {"_MaskMap"});

            //Initialize the new Terrain Layer
            newLayer.diffuseTexture   = tmpTerrainTexture;
            newLayer.normalMapTexture = tmpNormalsTexture;
            newLayer.maskMapTexture   = tmpMaskMapTexture;
            newLayer.metallic    = 0f;
            newLayer.smoothness  = 0f;
            newLayer.normalScale = 1f;
            newLayer.specular    = Color.black;
            newLayer.tileSize    = new Vector2(5f, 5f);
            newLayer.tileOffset  = Vector2.zero;


            
            AssetDatabase.CreateAsset(newLayer, TextureUtils.FullNameToUnityName(textureDir.FullName + "\\" + namePrefix + "_TerrainLayer.terrainlayer"));
            AssetDatabase.ImportAsset(TextureUtils.FullNameToUnityName(textureDir.FullName + "\\" + namePrefix + "_TerrainLayer.terrainlayer"), ImportAssetOptions.ForceUpdate);
        }

        //Apply Aniso Level to textures that were not newly written
        //Color Aniso
        if (Col != null)
        {
            TextureImporter textureImporter = (TextureImporter) TextureImporter.GetAtPath(AssetDatabase.GetAssetPath(Col));
            textureImporter.anisoLevel = AnisoLevel;
        }

        //Displacement Aniso
        if (Dis != null)
        {
            TextureImporter textureImporter = (TextureImporter) TextureImporter.GetAtPath(AssetDatabase.GetAssetPath(Dis));
            textureImporter.anisoLevel = AnisoLevel;
        }
        

        //Remove progress bar
        EditorUtility.ClearProgressBar();

        if (GenerateMaterial)
        {
            //Fix bug with materials, must be opened in Inspector to load properly idk why
            Selection.activeObject = newMaterial;
            EditorApplication.ExecuteMenuItem("Window/General/Inspector");
        }

        //Display elapsed time
        string timeText = ElapsedTimeTextFromTicks(renderStartTime);
        Debug.Log("Render Complete, Elapsed = " + timeText);
    }

    private void WriteToDisk(DirectoryInfo directory, string fileName, Texture2D texture)
    {
        WriteToDisk(directory, fileName, texture, false);
    }

    private void WriteToDisk(DirectoryInfo directory, string fileName, Texture2D texture, bool isNormal)
    {
        FileInfo outputFile = new FileInfo(directory.FullName + "\\" + fileName + ".png");

        //Actually write it to the disk
        File.WriteAllBytes(outputFile.FullName, texture.EncodeToPNG());

        //Load new asset into the database
        AssetDatabase.ImportAsset(TextureUtils.FullNameToUnityName(outputFile.FullName));

        //Get TextureImporter to change Aniso Level
        TextureImporter textureImporter = (TextureImporter) TextureImporter.GetAtPath(TextureUtils.FullNameToUnityName(outputFile.FullName));
        textureImporter.anisoLevel = AnisoLevel;
        
        //Apply settings if the texture is a Normal Map
        if (isNormal)
        {
            //Set type to Normal
            textureImporter.textureType = TextureImporterType.NormalMap;

            //Reimport as Normal
            AssetDatabase.ImportAsset(TextureUtils.FullNameToUnityName(outputFile.FullName), ImportAssetOptions.ForceUpdate);
        }
        
    }

    public void ExternAwake()
    {
        GenerateMaskMap          = EditorPrefs.GetBool(EDITOR_PREFS_PREFIX + "GenerateMaskMap"         , true);
        GenerateDetailMap        = EditorPrefs.GetBool(EDITOR_PREFS_PREFIX + "GenerateDetailMap"       , false);
        GenerateCorrectedNormals = EditorPrefs.GetBool(EDITOR_PREFS_PREFIX + "GenerateCorrectedNormals", true);
        GenerateMaterial         = EditorPrefs.GetBool(EDITOR_PREFS_PREFIX + "GenerateMaterial"        , true);
        GenerateTerrainLayer     = EditorPrefs.GetBool(EDITOR_PREFS_PREFIX + "GenerateTerrainLayer"    , false);
        DeleteLeftovers          = EditorPrefs.GetBool(EDITOR_PREFS_PREFIX + "DeleteLeftovers"         , true);
    }
    

    public void ExternOnDestroy()
    {
        EditorPrefs.SetBool(EDITOR_PREFS_PREFIX + "GenerateMaskMap"         , GenerateMaskMap);
        EditorPrefs.SetBool(EDITOR_PREFS_PREFIX + "GenerateDetailMap"       , GenerateDetailMap);
        EditorPrefs.SetBool(EDITOR_PREFS_PREFIX + "GenerateCorrectedNormals", GenerateCorrectedNormals);
        EditorPrefs.SetBool(EDITOR_PREFS_PREFIX + "GenerateMaterial"        , GenerateMaterial);
        EditorPrefs.SetBool(EDITOR_PREFS_PREFIX + "GenerateTerrainLayer"    , GenerateTerrainLayer);
        EditorPrefs.SetBool(EDITOR_PREFS_PREFIX + "DeleteLeftovers"         , DeleteLeftovers);
    }
    
}
