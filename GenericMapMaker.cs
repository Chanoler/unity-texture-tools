﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using static TextureUtils;

public class GenericMapMaker : TextureTool
{

    
    static readonly string EDITOR_PREFS_PREFIX = "GenericMap_Maker_";

    //Input textures, these are compressed by Unity
    //Most of the time I fetch a fresh copy from the disk when I need to actually do something with them
    GenericMapInput R;
    GenericMapInput G;
    GenericMapInput B;
    GenericMapInput A;

    bool ShowKeywords = false;
    string OutputFileName = "GenericMap";
    Vector2Int OutputFileResolution = new Vector2Int(4096, 4096);
    
    public GenericMapMaker()
    {
        /* Example
        GenericMapInput =
            Name,
            Editor Prefs Prefix,
            Default Value,
            Keywords,
            Channel from Texture to use
        )
        */

        R = new GenericMapInput(
            "Red Channel",
            "GENERIC_MAP_MAKER_RED_",
            1f,
            "Red",
            GenericMapInput.ColorChannel.R
        );

        G = new GenericMapInput(
            "Green Channel",
            "GENERIC_MAP_MAKER_GREEN_",
            1f,
            "Green",
            GenericMapInput.ColorChannel.G
        );

        B = new GenericMapInput(
            "Blue Channel",
            "GENERIC_MAP_MAKER_BLUE_",
            1f,
            "Blue",
            GenericMapInput.ColorChannel.B
        );

        A = new GenericMapInput(
            "Alpha Channel",
            "GENERIC_MAP_MAKER_ALPHA_",
            1f,
            "Alpha",
            GenericMapInput.ColorChannel.A
        );
    }

    public string GetName()
    {
        return "GenericMap Maker";
    }

    public string GetEditorPrefsPrefix()
    {
        return EDITOR_PREFS_PREFIX;
    }

    public void ExternOnGUI()
    {
        //--------------------------------------------------------------------------------------------------------------------------

        //Input Textures Label
        GUILayout.Label("Input Textures", EditorStyles.boldLabel);

        if(GUILayout.Button("Clear Textures"))
        {
            R.TextureIn = null;
            G.TextureIn = null;
            B.TextureIn = null;
            A.TextureIn = null;
        }
        if (GUILayout.Button("Reset Sliders"))
        {
            R.DefaultValue = 1f;
            G.DefaultValue = 1f;
            B.DefaultValue = 1f;
            A.DefaultValue = 1f;
        }
        EditorGUILayout.Space();
        
        //Display editor GUIs
        R.ExternOnGUI();
        EditorGUILayout.Space();
        G.ExternOnGUI();
        EditorGUILayout.Space();
        B.ExternOnGUI();
        EditorGUILayout.Space();
        A.ExternOnGUI();

        EditorGUILayout.Space();
        //--------------------------------------------------------------------------------------------------------------------------

        //Button to auto-detect remaining maps using Keywords
        if(GUILayout.Button(
            new GUIContent("Auto-detect other textures", "This button takes the first (top to bottom) defined texture, opens its directory, and searches other files in the directory for keywords that define other textures")
        ))
        {
            if (!HasNonNullTexture(R.TextureIn, G.TextureIn, B.TextureIn, A.TextureIn))
            {
                //No texture found, cancel
                EditorUtility.DisplayDialog("Error!", "Select at least one texture before attempting to run auto-detect", "Okay");
                goto CANCEL_AUTODETECT;
            }

            AutoDetectTextures();
        }
        CANCEL_AUTODETECT:

        if(GUILayout.Button(new GUIContent("Fill all texture slots", "This button takes the first (top to bottom) defined texture and fills all other slots with it")))
        {
            if (!HasNonNullTexture(R.TextureIn, G.TextureIn, B.TextureIn, A.TextureIn))
            {
                EditorUtility.DisplayDialog("Error!", "Please fill in at least one texture slot. Mouse over the button you just clicked for details.", "Okay");
                goto CANCEL_SLOT_FILL;
            }

            Texture2D nonNullTexture = GetFirstNonNullTexture(R.TextureIn, G.TextureIn, B.TextureIn, A.TextureIn);

            if (R.TextureIn == null)
                R.TextureIn = nonNullTexture;

            if (G.TextureIn == null)
                G.TextureIn = nonNullTexture;

            if (B.TextureIn == null)
                B.TextureIn = nonNullTexture;

            if (A.TextureIn == null)
                A.TextureIn = nonNullTexture;

        }
        CANCEL_SLOT_FILL:

        //Show or hide the list of keywords
        ShowKeywords = EditorGUILayout.Foldout(ShowKeywords, "Keywords for auto-detection");
        
        if (ShowKeywords)
        {
            GUILayout.Label("Case sensitive, separate with commas, hover over button above for details");
            R.Keywords = EditorGUILayout.TextField ("R", R.Keywords);
            G.Keywords = EditorGUILayout.TextField ("G", G.Keywords);
            B.Keywords = EditorGUILayout.TextField ("B", B.Keywords);
            A.Keywords = EditorGUILayout.TextField ("A", A.Keywords);
            if (GUILayout.Button("Reset Keywords"))
            {
                //Delete Keywords
                EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "RKeywords");
                EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "GKeywords"); 
                EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "BKeywords"); 
                EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "AKeywords"); 
                ExternAwake();
            }
            EditorGUILayout.Space();
        }


        //--------------------------------------------------------------------------------------------------------------------------

        //Label for Output Options
        EditorGUILayout.Space();
        GUILayout.Label("Output Options", EditorStyles.boldLabel);
        
        //Used to pick the output file name
        OutputFileName = EditorGUILayout.TextField("Output File Name", OutputFileName);
        
        //Begin horizontal resolution picker
        GUILayout.BeginHorizontal();
        GUILayout.Label("Output Resolution");
        OutputFileResolution = EditorGUILayout.Vector2IntField("", OutputFileResolution);
        EditorGUILayout.EndHorizontal();

        if(GUILayout.Button(new GUIContent("Auto-set resoltion", "Reads the original texture asset fromt he disk and sets the output resolution equal to the input texture with the largest area")))
        {
            if (!HasNonNullTexture(R.TextureIn, G.TextureIn, B.TextureIn, A.TextureIn))
            {
                Debug.LogWarning("Select at least one texture before attempting to auto-set resolution");
                goto CANCEL_AUTORES;
            }

            OutputFileResolution = AutoResolution(R.TextureIn, G.TextureIn, B.TextureIn, A.TextureIn);
        }
        CANCEL_AUTORES:

        //Render button
        if(GUILayout.Button("Render and Save Texture"))
        {
            RenderMap();
        }
        
    }

    private void RenderMap()
    {
        long renderStartTime = System.DateTime.Now.Ticks;

        //Texture to render to
        Texture2D OutputTexture = new Texture2D(OutputFileResolution.x, OutputFileResolution.y);

        //Easy access to OutputTexture data
        int width  = OutputTexture.width;
        int height = OutputTexture.height;
        int length = width * height;

        //Get pixel data straight from the disk
        EditorUtility.DisplayProgressBar("Rendering...", "Reading and preparing textures", 0f);
        Color[] RPixels = TextureUtils.GetPixelDataFromDisk(OutputFileResolution, R.DefaultValue, R.TextureIn);
        EditorUtility.DisplayProgressBar("Rendering...", "Reading and preparing textures", 0.25f);
        Color[] GPixels = TextureUtils.GetPixelDataFromDisk(OutputFileResolution, G.DefaultValue, G.TextureIn);
        EditorUtility.DisplayProgressBar("Rendering...", "Reading and preparing textures", 0.50f);
        Color[] BPixels = TextureUtils.GetPixelDataFromDisk(OutputFileResolution, B.DefaultValue, B.TextureIn);
        EditorUtility.DisplayProgressBar("Rendering...", "Reading and preparing textures", 0.75f);
        Color[] APixels = TextureUtils.GetPixelDataFromDisk(OutputFileResolution, A.DefaultValue, A.TextureIn);

        Color[] OutPixels = new Color[length];

        //Used so the progress bar only updates 100 times
        int lengthDiv100 = length / 100;

        for (int i = 0; i < length; i++)
        {
            OutPixels[i].r = TextureUtils.GetChannel(R.inputChannel, RPixels[i]);
            OutPixels[i].g = TextureUtils.GetChannel(G.inputChannel, GPixels[i]);
            OutPixels[i].b = TextureUtils.GetChannel(B.inputChannel, BPixels[i]);
            OutPixels[i].a = TextureUtils.GetChannel(A.inputChannel, APixels[i]);
            //Output 100 times
            if (i % lengthDiv100 == 0)
                EditorUtility.DisplayProgressBar("Rendering...", "Rendering (" + i + "/" + length + ")", (float) i / (float) length);
        }

        //Debug
        EditorUtility.DisplayProgressBar("Rendering...", "Writing texture data", 1f);

        //Apply the modified pixels to the image
        OutputTexture.SetPixels(OutPixels);
        OutputTexture.Apply();

        //Decide where it goes
        DirectoryInfo outputDiectory = GetDirectoryFromFirstNonNullTexture(R.TextureIn, G.TextureIn, B.TextureIn, A.TextureIn);
        FileInfo outputFile = new FileInfo(outputDiectory.FullName + "/" + OutputFileName + ".png");

        //Actually write it to the disk
        File.WriteAllBytes(outputFile.FullName, OutputTexture.EncodeToPNG());

        //Inform user of success EDIT: This is done later, not needed
        //Debug.Log("Wrote file \"" + outputFile.FullName + "\"");

        //Import the new file
        AssetDatabase.ImportAsset(TextureUtils.FullNameToUnityName(outputFile.FullName));

        EditorUtility.ClearProgressBar();

        //Display elapsed time
        string timeText = ElapsedTimeTextFromTicks(renderStartTime);
        Debug.Log("Render Complete, Elapsed = " + timeText + "; Written to \"" + outputFile.FullName + "\"");
    }

    private void AutoDetectTextures()
    {
        DirectoryInfo textureDir = GetDirectoryFromFirstNonNullTexture(R.TextureIn, G.TextureIn, B.TextureIn, A.TextureIn);

        //Replace null textures with the detected texture

        //R
        if (R.TextureIn == null) {
            //First get the file
            FileInfo DetectedRFile = fileWithKeyword(textureDir, R.Keywords.Split(','));
            //Load it
            R.TextureIn = DetectedRFile == null ? null : AssetDatabase.LoadAssetAtPath<Texture2D>(TextureUtils.FullNameToUnityName(DetectedRFile.FullName));
            //Print status
            Debug.Log(
                R.TextureIn == null ?
                "No suitable R texture found" :
                "Found R Texture: " + R.TextureIn.name
            );
        } else Debug.Log("Using Existing R: " + R.TextureIn.name);

        //G
        if (G.TextureIn == null) {
            //First get the file
            FileInfo DetectedGFile = fileWithKeyword(textureDir, G.Keywords.Split(','));
            //Load it
            G.TextureIn = DetectedGFile == null ? null : AssetDatabase.LoadAssetAtPath<Texture2D>(TextureUtils.FullNameToUnityName(DetectedGFile.FullName));
            //Print status
            Debug.Log(
                G.TextureIn == null ?
                "No suitable G texture found" :
                "Found G Texture: " + G.name
            );
        } else Debug.Log("Using Existing G: " + G.name);

        //B
        if (B.TextureIn == null) {
            //First get the file
            FileInfo DetectedBFile = fileWithKeyword(textureDir, B.Keywords.Split(','));
            //Load it
            B.TextureIn = DetectedBFile == null ? null : AssetDatabase.LoadAssetAtPath<Texture2D>(TextureUtils.FullNameToUnityName(DetectedBFile.FullName));
            //Print status
            Debug.Log(
                B.TextureIn == null ?
                "No suitable B texture found" :
                "Found B Texture: " + B.name
            );
        } else Debug.Log("Using Existing B: " + B.name);

        //Detail
        if (A.TextureIn == null) {
            //First get the file
            FileInfo DetectedAFile = fileWithKeyword(textureDir, A.Keywords.Split(','));
            //Load it
            A.TextureIn = DetectedAFile == null ? null : AssetDatabase.LoadAssetAtPath<Texture2D>(TextureUtils.FullNameToUnityName(DetectedAFile.FullName));
            //Print status
            Debug.Log(
                A.TextureIn == null ?
                "No suitable A texture found" :
                "Found A Texture: " + A.name
            );
        } else Debug.Log("Using Existing A: " + A.name);
    }

    public void ExternAwake() {
        //Load persistent data
        //Keywords
        R.Keywords = EditorPrefs.GetString(EDITOR_PREFS_PREFIX + "RKeywords", "Red");
        G.Keywords = EditorPrefs.GetString(EDITOR_PREFS_PREFIX + "GKeywords", "Green");
        B.Keywords = EditorPrefs.GetString(EDITOR_PREFS_PREFIX + "BKeywords", "Blue");
        A.Keywords = EditorPrefs.GetString(EDITOR_PREFS_PREFIX + "AKeywords", "Green");

        //Resolution preferences
        OutputFileResolution = new Vector2Int(
            EditorPrefs.GetInt(EDITOR_PREFS_PREFIX + "OutputFileResolutionX", 4096),
            EditorPrefs.GetInt(EDITOR_PREFS_PREFIX + "OutputFileResolutionY", 4096)
        );

        //Default Value when missing texture
        R.DefaultValue = EditorPrefs.GetFloat(EDITOR_PREFS_PREFIX + "DefaultRValue", 0f);
        G.DefaultValue = EditorPrefs.GetFloat(EDITOR_PREFS_PREFIX + "DefaultGValue", 1f);
        B.DefaultValue = EditorPrefs.GetFloat(EDITOR_PREFS_PREFIX + "DefaultBValue", 1f);
        A.DefaultValue = EditorPrefs.GetFloat(EDITOR_PREFS_PREFIX + "DefaultAValue", 1f);


    }

    public void ExternOnDestroy() {
        //Write persistent data
        //Keywords
        EditorPrefs.SetString(EDITOR_PREFS_PREFIX + "RKeywords", R.Keywords);
        EditorPrefs.SetString(EDITOR_PREFS_PREFIX + "GKeywords", G.Keywords);
        EditorPrefs.SetString(EDITOR_PREFS_PREFIX + "BKeywords", B.Keywords);
        EditorPrefs.SetString(EDITOR_PREFS_PREFIX + "AKeywords", A.Keywords);

        //Resolution preferences
        EditorPrefs.SetInt(EDITOR_PREFS_PREFIX + "OutputFileResolutionX", OutputFileResolution.x);
        EditorPrefs.SetInt(EDITOR_PREFS_PREFIX + "OutputFileResolutionY", OutputFileResolution.y);

        //Default Value when missing texture
        EditorPrefs.SetFloat(EDITOR_PREFS_PREFIX + "DefaultRValue", R.DefaultValue);
        EditorPrefs.SetFloat(EDITOR_PREFS_PREFIX + "DefaultGValue", G.DefaultValue);
        EditorPrefs.SetFloat(EDITOR_PREFS_PREFIX + "DefaultBValue", B.DefaultValue);
        EditorPrefs.SetFloat(EDITOR_PREFS_PREFIX + "DefaultAValue", A.DefaultValue);
    }

    //[MenuItem("Window/Chan's Texture Tools/Clear Preferences for MaskMaker")]
    /*public void ResetPreferences()
    {
        //Write persistent data
        //Keywords
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "RKeywords"  );
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "GKeywords");
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "BKeywords" );
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "AKeywords");

        //R preferences
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "RAsG");

        //Resolution preferences
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "OutputFileResolutionX");
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "OutputFileResolutionY");

        //Default Value when missing texture
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "DefaultRValue"  );
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "DefaultGValue");
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "DefaultBValue" );
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "DefaultAValue");

        ExternAwake();
    }*/

}