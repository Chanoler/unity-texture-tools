﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class UnityTextureTools : EditorWindow
{
    static readonly string EDITOR_PREFS_PREFIX = "Texture_Map_Maker_";
    
    TextureTool[] textureTools = {
        new MaskMapMaker(),
        new DetailMapMaker(),
        new GenericMapMaker(),
        new cc0texturesDotComConverter(),
        new ExtraTools()
    };

    //isEnabled.Length keeps returning 2 ;w;
    bool[] isEnabled;
    
    Vector2 ScrollStatus = Vector2.zero;
    float screenHeight = Screen.height;
    float screenWidth  = Screen.width;

    [MenuItem("Window/Unity Texture Tools")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow.GetWindow(typeof(UnityTextureTools));
    }

    void OnGUI()
    {
        //Check for correct installation
        //Get GUID for this script
        string[] selfGUID = AssetDatabase.FindAssets ("t:Script Unity Texture Tools Main");
        if (selfGUID.Length != 0)
        {
            //If found, convert to path
            string selfPath = AssetDatabase.GUIDToAssetPath(selfGUID[0]);
            //Convert to lower case for name checking
            selfPath = selfPath.ToLower();

            //Print if not Editor
            if (!selfPath.StartsWith("editor/") && !selfPath.Contains("/editor/"))
            {
                GUIStyle errorStyle = new GUIStyle(EditorStyles.textField);
                errorStyle.normal.textColor = Color.red;
                errorStyle.wordWrap = true;
                EditorGUILayout.LabelField("This tool should be in a folder named \"Editor\" or in the subfolder of an \"Editor\" folder. The current setup will cause build errors.", errorStyle);
            }
        }
        

        //Sanity check, yes I actually need this.
        if (isEnabled.Length != textureTools.Length)
            Awake();

        //Draw editor GUIs
        ScrollStatus = EditorGUILayout.BeginScrollView(ScrollStatus);
        EditorGUILayout.BeginVertical();
        for (int i = 0; i < textureTools.Length; i++)
        {
            TextureTool textureTool = textureTools[i];

            TextureUtils.GuiLine();
            //State enabler
            isEnabled[i] = EditorGUILayout.Foldout(isEnabled[i], textureTool.GetName());
            
            if (isEnabled[i]) 
            {
                TextureUtils.GuiLine();
                //Show if enabled
                textureTool.ExternOnGUI();
                //Spacing for next element
                TextureUtils.Space();
            }
        }
        EditorGUILayout.EndVertical();
        EditorGUILayout.EndScrollView();
    }

    void Awake() {
        isEnabled = new bool[textureTools.Length];

        //Run awake actions, remember enabled state
        for (int i = 0; i < textureTools.Length; i++)
        {
            TextureTool textureTool = textureTools[i];
            isEnabled[i] = EditorPrefs.GetBool(EDITOR_PREFS_PREFIX + textureTool.GetEditorPrefsPrefix() + "_Enabled", false);
            textureTool.ExternAwake();
        }
    }

    void OnDestroy() {
        //Run destroy actions, remember enabled state
        for (int i = 0; i < textureTools.Length; i++)
        {
            TextureTool textureTool = textureTools[i];
            EditorPrefs.SetBool(EDITOR_PREFS_PREFIX + textureTool.GetEditorPrefsPrefix() + "_Enabled", isEnabled[i]);
            textureTool.ExternOnDestroy();
        }
    }

    
}
