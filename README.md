# Unity Texture Tools
Primarily for converting non-specific downloaded textures into formats Unity is more familiar with.

I like downloading textures for use in my Unity projects but it comes with a number of issues, [cc0textures.com](https://cc0textures.com/list?sort=Popular) (great site btw, highly recommend) for example has:
<ol>
<li> DirectX-style Normals, Unity wants OpenGL-style</li>
<li> Separate Metallic and Smoothness maps</li>
<li> Roughness, Unity wants Smoothness which is inverted</li>
</ol>
So I went and did what programmers do best, I spent hours writing a tool to save minutes of manual work, hopefully my hours can save you minutes too.
I need feedback to improve this program, if it sucks PLEASE tell me why it sucks and how I can make it suck less. Email me at [xX.Chanoler.Xx@gmail.com](mailto:xX.Chanoler.Xx@gmail.com)

# Installation
You can install this by either downloading "master" and placing it in the "Assets/Editor/Unity Texture Tools" folder or add it as a git submodule with this comment:

    git submodule add https://gitlab.com/Chanoler/unity-texture-tools.git "Assets/Editor/Unity Texture Tools"

Update by either deleting its folder and reinstalling it from the above instructions or with the following command if you installed it as a submodule (also updates your other submodules, hopefully you want that)

    git submodule foreach git pull origin master


# Compatibility
This editor tool was designed using Unity 2018.4.20f1. I make no guarentees as to compatibility with older or newer versions but hey give it a shot
