﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using static TextureUtils;


public class GenericMapInput : TextureTool
{
    //Name of the actual input channel
    public string Name { get; private set; }
    //Name of the input texture
    public string name { get { return TextureIn.name; } }
    public string EDITOR_PREFS_PREFIX { get; private set; }
    public Texture2D TextureIn;
    public float DefaultValue;
    public string Keywords;

    public ColorChannel inputChannel;
    private ColorChannel defaultInputChannel;
    //private ColorChannel outputChannel;
    
    public enum ColorChannel {
        R,
        G,
        B,
        A,
        Grayscale,
        R_Inverted,
        G_Inverted,
        B_Inverted,
        A_Inverted,
        Grayscale_Inverted
    }

    public GenericMapInput(string name, string editorPrefsPrefix, float defaultValue, string keywords, ColorChannel inputChannel)
    {
        this.Name = name;
        this.EDITOR_PREFS_PREFIX = editorPrefsPrefix;
        this.DefaultValue = defaultValue;
        this.Keywords = keywords;
        this.inputChannel = inputChannel;
        this.defaultInputChannel = inputChannel;
        //this.outputChannel = outputChannel;
    }

    public string GetName() { return Name; }

    public string GetEditorPrefsPrefix() { return EDITOR_PREFS_PREFIX; }
    
    public void ExternOnGUI()
    {
        
        //ChannelTexture Input
        TextureIn = (Texture2D) EditorGUILayout.ObjectField(Name, TextureIn, typeof (Texture2D), false); 
        //ChannelTexture Default
        if (TextureIn == null)
        {
            DefaultValue = EditorGUILayout.Slider(new GUIContent(""), DefaultValue, 0f, 1f);
            inputChannel = defaultInputChannel;
        }
        else
        {
            //GUI.enabled = false;
            GUILayout.BeginHorizontal();
            GUILayout.Label("Using", GUILayout.Width(35));
            //GUI.enabled = true;
            inputChannel = (ColorChannel) EditorGUILayout.EnumPopup(inputChannel, GUILayout.ExpandWidth(false));
            //GUI.enabled = false;
            GUILayout.Label("channel from " + TextureIn.name);
            GUILayout.EndHorizontal();
            //GUI.enabled = true;
        }
    }
    
    public void ExternAwake()
    {
        //Serialization not available yet
    }
    
    public void ExternOnDestroy()
    {
        //Serialization not available yet
    }
}
