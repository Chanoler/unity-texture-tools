using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;


public static class TextureUtils
{
    public static string FullNameToUnityName(string FullName)
    {
        if (FullName.StartsWith("Assets\\"))
            return FullName;
        else
            return FullName.Substring(FullName.IndexOf("Assets\\") == -1 ? 0 : FullName.IndexOf("Assets\\"));
    }

    //Takes a unity texture and loads it uncompressed from the disk
    public static Texture2D GetTextureFromDisk(Texture2D unityTexture)
    {
        if (unityTexture == null)
            return null;

        //Get the path for this asset
        string texturePath = AssetDatabase.GetAssetPath(unityTexture);

        //Initialize placeholder texture
        Texture2D tmpTexture = new Texture2D(2, 2);
        
        //Load from disk
        tmpTexture.LoadImage(File.ReadAllBytes(texturePath));

        return tmpTexture;
    }

    //https://forum.unity.com/threads/horizontal-line-in-editor-window.520812/
    //Used to draw a line separator in the Unity editor
    public static void GuiLine( int i_height = 1 )
    {
       Rect rect = EditorGUILayout.GetControlRect(false, i_height );
       rect.height = i_height;
       EditorGUI.DrawRect(rect, new Color ( 0.5f,0.5f,0.5f, 1 ) );

   }

   public static void Space()
   {
       EditorGUILayout.Space();
   }

   public static Color ToGrayscale(Color color)
   {
        float average = color.r + color.g + color.b;
        average /= 3;


        return new Color(
           average,
           average,
           average
       );
   }

   public static float ToGrayscaleFloat(Color color)
   {
        float average = color.r + color.g + color.b;
        average /= 3;

        return average;
   }

   public static Color[] GetOrFakePixels (int width, int height, float defaultValue, Texture2D texture)
   {
        if (texture != null)
            return texture.GetPixels();

        Color[] toReturn = new Color[width * height];
        
        Color fakePixel = new Color(defaultValue, defaultValue, defaultValue, defaultValue);

        for (int i = 0; i < toReturn.Length; i++)
        {
            toReturn[i] = fakePixel;
        }

        return toReturn;
   }

   public static float GetChannel(GenericMapInput.ColorChannel channel, Color color)
   {
       switch (channel)
       {
        case GenericMapInput.ColorChannel.R:
            return color.r;
        case GenericMapInput.ColorChannel.G:
            return color.g;
        case GenericMapInput.ColorChannel.B:
            return color.b;
        case GenericMapInput.ColorChannel.A:
            return color.a;
        case GenericMapInput.ColorChannel.Grayscale:
            return TextureUtils.ToGrayscaleFloat(color);
        case GenericMapInput.ColorChannel.R_Inverted:
            return 1f - color.r;
        case GenericMapInput.ColorChannel.G_Inverted:
            return 1f - color.g;
        case GenericMapInput.ColorChannel.B_Inverted:
            return 1f - color.b;
        case GenericMapInput.ColorChannel.A_Inverted:
            return 1f - color.a;
        case GenericMapInput.ColorChannel.Grayscale_Inverted:
            return 1f - TextureUtils.ToGrayscaleFloat(color);
        default:
            throw new System.Exception("Unhandled enum");
       }
   }
   
    //Repeated functions ripped out of their classes because I'm tired of reimplementing them

   public static void Resize(Texture2D texture, int width, int height)
   {
        TextureScale.Bilinear(texture, width, height);
   }
    
   public static Texture2D AutoDetectTexture(DirectoryInfo textureDir, string[] textureKeywords)
   {
        //First get the file
        FileInfo DetectedtextureFile = fileWithKeyword(textureDir, textureKeywords);
        //Load it
        return DetectedtextureFile == null ? null : AssetDatabase.LoadAssetAtPath<Texture2D>(TextureUtils.FullNameToUnityName(DetectedtextureFile.FullName));
   }

   //Search for a file in the given directory using a list of keywords
    public static FileInfo fileWithKeyword (DirectoryInfo directory, string[] keywords)
    {
        //Iterate through all files in the directory
        foreach (FileInfo file in directory.GetFiles())
        {
            //Check if that file contains a keyword
            if (file.Extension != ".meta" && containsKeyword(file.Name, keywords))
                return file;
        }

        return null;
    }

    //Checks if the first argument contains any of the entries in the second argument
    public static bool containsKeyword (string searchThis, string[] keywords)
	{
		foreach (string keyword in keywords)
		{
			if (searchThis.Contains(keyword.Trim()))
				return true;
		}
		
		return false;
	}

    public static Texture2D GetFirstNonNullTexture(params Texture2D[] textures)
    {
        Debug.Assert(textures.Length > 0, "Improper override, minimum 1 argument");
        
        //Search for the first non-null texture
        Texture2D nonNullTexture = null;

        foreach (Texture2D texture in textures)
        {
            if (nonNullTexture == null && texture != null)
            {
                nonNullTexture = texture;
                goto TEXTURE_FOUND;
            }
        }
        TEXTURE_FOUND:

        return nonNullTexture;
    }

    public static DirectoryInfo GetDirectoryFromFirstNonNullTexture(params Texture2D[] textures)
    {
        Debug.Assert(textures.Length > 0, "Improper override, minimum 1 argument");

        //Path of first non-null texture
        string texturePath = AssetDatabase.GetAssetPath(GetFirstNonNullTexture(textures));
        //Directory containing textures
        return new FileInfo(texturePath).Directory;
    }

    public static bool HasNonNullTexture(params Texture2D[] textures)
    {
        Debug.Assert(textures.Length > 0, "Improper override, minimum 1 argument");

        foreach (Texture2D texture in textures)
        {
            if (texture != null)
                return true;
        }

        return false;
    }

    public static Vector2Int AutoResolution(params Texture2D[] textures)
    {
        Debug.Assert(textures.Length > 0, "Improper override, minimum 1 argument");

        int biggestArea = 0;

        Vector2Int highestRes = Vector2Int.zero;

        foreach (Texture2D texture in textures)
        {
            Texture2D rawTexture = TextureUtils.GetTextureFromDisk(texture);
            
            if (rawTexture != null && rawTexture.width * rawTexture.height > biggestArea)
            {
                biggestArea = rawTexture.width * rawTexture.height;
                highestRes = new Vector2Int(rawTexture.width, rawTexture.height);
            }
        }
        
        return highestRes;
    }

    public static Color[] GetPixelDataFromDisk(
        Vector2Int ScaleToResolution,
        float colorIfNull,
        Texture2D unityTexture
    )
    {
        //Initialize placeholder textures, null if original is not set
        Texture2D TextureFromDisk   = TextureUtils.GetTextureFromDisk(unityTexture);

        //Rezise maps as necessary
        if (TextureFromDisk != null)
            if(TextureFromDisk.width != ScaleToResolution.x || TextureFromDisk.height != ScaleToResolution.y)
                TextureScale.Bilinear(TextureFromDisk, ScaleToResolution.x, ScaleToResolution.y);


        Color[] pixels = TextureUtils.GetOrFakePixels(ScaleToResolution.x, ScaleToResolution.y, colorIfNull, TextureFromDisk);

        return pixels;
    }

    //Text describing the time elapsed between the start time and now
    public static string ElapsedTimeTextFromTicks(long startTicks)
    {
        return ElapsedTimeTextFromTicks(startTicks, System.DateTime.Now.Ticks);
    }

    //Text describing the time elapsed between two points in time
    public static string ElapsedTimeTextFromTicks(long startTicks, long endTicks)
    {
        //Calculate elapsed time for rendering
        long renderTime = endTicks - startTicks;
        TimeSpan timeSpan = TimeSpan.FromTicks(renderTime);
        
        //Format and return
        return string.Format("{0:D2}:{1:D2}.{2:D3}", timeSpan.Minutes, timeSpan.Seconds, timeSpan.Milliseconds);
    }
}
