﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using static TextureUtils;

public class MaskMapMaker : TextureTool
{
    static readonly string EDITOR_PREFS_PREFIX = "MaskMap_Maker_";

    //Input textures, these are compressed by Unity
    //Most of the time I fetch a fresh copy from the disk when I need to actually do something with them
    Texture2D Metallic = null;
    Texture2D Smoothness = null;
    Texture2D Occlusion = null;
    Texture2D DetailMask = null;
    bool MetallicAsSmoothness = false;
    bool InvertSmoothness = false;

    float DefaultMetallicValue = 0f;
    float DefaultSmoothnessValue = 1f;
    float DefaultOcclusionValue = 1f;
    float DefaultDetailMaskValue = 1f;

    bool ShowKeywords = false;
    string MetallicKeywords = "Metallic, Metalness, Metal";
    string SmoothnessKeywords = "Smoothness, Roughness";
    string OcclusionKeywords = "Occlusion, AO";
    string DetailMaskKeywords = "Detail";

    string OutputFileName = "MaskMap";
    Vector2Int OutputFileResolution = new Vector2Int(4096, 4096);
    
    public MaskMapMaker()
    {

    }

    public string GetName()
    {
        return "MaskMap Maker";
    }

    public string GetEditorPrefsPrefix()
    {
        return EDITOR_PREFS_PREFIX;
    }

    public void ExternOnGUI()
    {
        //--------------------------------------------------------------------------------------------------------------------------

        //Input Textures Label
        GUILayout.Label("Input Textures", EditorStyles.boldLabel);

        if(GUILayout.Button("Clear Textures"))
        {
            Metallic = null;
            Smoothness = null;
            Occlusion = null;
            DetailMask = null;
        }
        if (GUILayout.Button("Reset Sliders"))
        {
            DefaultMetallicValue   = 0f;
            DefaultSmoothnessValue = 1f;
            DefaultOcclusionValue  = 1f;
            DefaultDetailMaskValue = 1f;
        }
        EditorGUILayout.Space();
        

        //Metalness map input
        Metallic = (Texture2D) EditorGUILayout.ObjectField("Metallic " + (MetallicAsSmoothness ? "(R, A)" : "(R)"), Metallic, typeof (Texture2D), false);
        //Metallic Default
        if (Metallic == null)
            DefaultMetallicValue = EditorGUILayout.Slider(new GUIContent(""), DefaultMetallicValue, 0f, 1f);
        else
            {
                GUI.enabled = false;
                GUILayout.Label("Using " + (MetallicAsSmoothness ? "R and A channels" : "R channel" )+ " from " + Metallic.name);
                GUI.enabled = true;
            }

        //Dropdown to pick how to treat alpha on metalness
        EditorGUILayout.Space();
        string[] metallicOptions = {"Use Metallic Alpha", "Red or grayscale Smoothness Texture"};
        MetallicAsSmoothness = EditorGUILayout.Popup("Smoothness Mode", MetallicAsSmoothness ? 0 : 1, metallicOptions) == 0; 
        //Disable Smoothness map if using Metallic Alpha
        if (!MetallicAsSmoothness)
        {
            Smoothness = (Texture2D) EditorGUILayout.ObjectField("Smoothness Texture (A)", Smoothness, typeof (Texture2D), false); 
            //Smoothness Default
            if (Smoothness == null)
                DefaultSmoothnessValue = EditorGUILayout.Slider(new GUIContent(""), DefaultSmoothnessValue, 0f, 1f);
            else
                {
                    GUI.enabled = false;
                    GUILayout.Label("Using R channel from" + Smoothness.name);
                    GUI.enabled = true;
                    InvertSmoothness = EditorGUILayout.Toggle("Invert Smoothness", InvertSmoothness, GUILayout.ExpandWidth(false));
                }
        }
        else
        {
            Smoothness = null;
        }
        

        EditorGUILayout.Space();
        //Occlusion Map Input
        Occlusion = (Texture2D) EditorGUILayout.ObjectField("Occlusion (G)", Occlusion, typeof (Texture2D), false); 
        //Occlusion Default
        if (Occlusion == null)
            DefaultOcclusionValue = EditorGUILayout.Slider(new GUIContent(""), DefaultOcclusionValue, 0f, 1f);
        else
        {
            GUI.enabled = false;
            GUILayout.Label("Using G channel from " + Occlusion.name);
            GUI.enabled = true;
        }
        
        //Detail Mask Map Input
        EditorGUILayout.Space();
        DetailMask = (Texture2D) EditorGUILayout.ObjectField("Detail mask (B)", DetailMask, typeof (Texture2D), false); 
        //DetailMask Default
        if (DetailMask == null)
            DefaultDetailMaskValue = EditorGUILayout.Slider(new GUIContent(""), DefaultDetailMaskValue, 0f, 1f);
        else
        {
            GUI.enabled = false;
            GUILayout.Label("Using B channel from" + DetailMask.name);
            GUI.enabled = true;
        }

        EditorGUILayout.Space();
        //--------------------------------------------------------------------------------------------------------------------------

        //Button to auto-detect remaining maps using Keywords
        if(GUILayout.Button(
            new GUIContent("Auto-detect other textures", "This button takes the first (top to bottom) defined texture, opens its directory, and searches other files in the directory for keywords that define other textures")
        ))
        {
            if (!HasNonNullTexture(Metallic, Smoothness, Occlusion, DetailMask))
            {
                //No texture found, cancel
                Debug.LogWarning("Select at least one texture before attempting to run auto-detect");
                goto CANCEL_AUTODETECT;
            }

            AutoDetectTextures();
        }
        CANCEL_AUTODETECT:

        //Show or hide the list of keywords
        ShowKeywords = EditorGUILayout.Foldout(ShowKeywords, "Keywords for auto-detection");
        
        if (ShowKeywords)
        {
            GUILayout.Label("Case sensitive, separate with commas, hover over button above for details");
            MetallicKeywords   = EditorGUILayout.TextField ("Metallic", MetallicKeywords);
            SmoothnessKeywords = EditorGUILayout.TextField ("Smoothness", SmoothnessKeywords);
            OcclusionKeywords  = EditorGUILayout.TextField ("Occlusion", OcclusionKeywords);
            DetailMaskKeywords = EditorGUILayout.TextField ("DetailMask", DetailMaskKeywords);
            if (GUILayout.Button("Reset Keywords"))
            {
                //Delete Keywords
                EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "MetallicKeywords");
                EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "SmoothnessKeywords"); 
                EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "OcclusionKeywords"); 
                EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "DetailMaskKeywords"); 
                ExternAwake();
            }
            EditorGUILayout.Space();
        }


        //--------------------------------------------------------------------------------------------------------------------------

        //Label for Output Options
        EditorGUILayout.Space();
        GUILayout.Label("Output Options", EditorStyles.boldLabel);
        
        //Used to pick the output file name
        OutputFileName = EditorGUILayout.TextField("Output File Name", OutputFileName);
        
        //Begin horizontal resolution picker
        GUILayout.BeginHorizontal();
        GUILayout.Label("Output Resolution");
        OutputFileResolution = EditorGUILayout.Vector2IntField("", OutputFileResolution);
        EditorGUILayout.EndHorizontal();

        if(GUILayout.Button(new GUIContent("Auto-set resoltion", "Reads the original texture asset fromt he disk and sets the output resolution equal to the input texture with the largest area")))
        {
            if (!HasNonNullTexture(Metallic, Smoothness, Occlusion, DetailMask))
            {
                Debug.LogWarning("Select at least one texture before attempting to auto-set resolution");
                goto CANCEL_AUTORES;
            }

            OutputFileResolution = AutoResolution(Metallic, Smoothness, Occlusion, DetailMask);
        }
        CANCEL_AUTORES:

        //Render button
        if(GUILayout.Button("Render and Save MaskMap"))
        {
            RenderMap();
        }
        
    }

    private void RenderMap()
    {
        long renderStartTime = System.DateTime.Now.Ticks;

        //Texture to render to
        Texture2D OutputTexture = new Texture2D(OutputFileResolution.x, OutputFileResolution.y);

        //Easy access to OutputTexture data
        int width  = OutputTexture.width;
        int height = OutputTexture.height;
        int length = width * height;

        //Get pixel data straight from the disk
        EditorUtility.DisplayProgressBar("Rendering...", "Reading and preparing textures", 0f);
        Color[] MetalPixels = TextureUtils.GetPixelDataFromDisk(OutputFileResolution, DefaultMetallicValue, Metallic);
        EditorUtility.DisplayProgressBar("Rendering...", "Reading and preparing textures", 0.25f);
        Color[] SmootPixels = TextureUtils.GetPixelDataFromDisk(OutputFileResolution, DefaultSmoothnessValue, Smoothness);
        EditorUtility.DisplayProgressBar("Rendering...", "Reading and preparing textures", 0.50f);
        Color[] OccluPixels = TextureUtils.GetPixelDataFromDisk(OutputFileResolution, DefaultOcclusionValue, Occlusion);
        EditorUtility.DisplayProgressBar("Rendering...", "Reading and preparing textures", 0.75f);
        Color[] DetaiPixels = TextureUtils.GetPixelDataFromDisk(OutputFileResolution, DefaultDetailMaskValue, DetailMask);

        Color[] OutPixels = new Color[length];

        //Used so the progress bar only updates 100 times
        int lengthDiv100 = length / 100;

        for (int i = 0; i < length; i++)
        {
            //Metal on R channel
            OutPixels[i].r = MetalPixels[i].r;
            //Occlusion on G channel
            OutPixels[i].g = OccluPixels[i].g;
            //DetailMask on B channel
            OutPixels[i].b = DetaiPixels[i].b;
            //Smoothness on A channel
            OutPixels[i].a = MetallicAsSmoothness ? MetalPixels[i].a : SmootPixels[i].r;

            if (InvertSmoothness)
                OutPixels[i].a = 1f - OutPixels[i].a;

            //Output every 256 pixel updates
            if (i % lengthDiv100 == 0)
                EditorUtility.DisplayProgressBar("Rendering...", "Rendering (" + i + "/" + length + ")", (float) i / (float) length);
        }

        //Debug
        EditorUtility.DisplayProgressBar("Rendering...", "Writing texture data", 1f);

        //Apply the modified pixels to the image
        OutputTexture.SetPixels(OutPixels);
        OutputTexture.Apply();

        //Decide where it goes
        DirectoryInfo outputDiectory = GetDirectoryFromFirstNonNullTexture(Metallic, Smoothness, Occlusion, DetailMask);
        FileInfo outputFile = new FileInfo(outputDiectory.FullName + "/" + OutputFileName + ".png");

        //Actually write it to the disk
        File.WriteAllBytes(outputFile.FullName, OutputTexture.EncodeToPNG());

        //Inform user of success EDIT: This is done later, not needed
        //Debug.Log("Wrote file \"" + outputFile.FullName + "\"");

        //Import the new file
        AssetDatabase.ImportAsset(TextureUtils.FullNameToUnityName(outputFile.FullName));

        //Remove progress bar
        EditorUtility.ClearProgressBar();

        //Display elapsed time
        string timeText = ElapsedTimeTextFromTicks(renderStartTime);
        Debug.Log("Render Complete, Elapsed = " + timeText + "; Written to \"" + outputFile.FullName + "\"");
    }

    private void AutoDetectTextures()
    {
        DirectoryInfo textureDir = GetDirectoryFromFirstNonNullTexture(Metallic, Smoothness, Occlusion, DetailMask);

        //Replace null textures with the detected texture

        //Metallic
        if (Metallic   == null) Metallic   = TextureUtils.AutoDetectTexture(textureDir, MetallicKeywords.Split(','));
        if (Smoothness == null) Smoothness = TextureUtils.AutoDetectTexture(textureDir, SmoothnessKeywords.Split(','));
        if (Occlusion  == null) Occlusion  = TextureUtils.AutoDetectTexture(textureDir, OcclusionKeywords.Split(','));
        if (DetailMask == null) DetailMask = TextureUtils.AutoDetectTexture(textureDir, DetailMaskKeywords.Split(','));

        //Debug prints
        /*
        //Metallic
        if (Metallic == null) {
            //Print status
            Debug.Log(
                Metallic == null ?
                "No suitable Metallic texture found" :
                "Found Metallic Texture: " + Metallic.name
            );
        } else Debug.Log("Using Metallic: " + Metallic.name);

        //Smoothness
        if (Smoothness == null) {
            //Print status
            Debug.Log(
                Smoothness == null ?
                "No suitable Smoothness texture found" :
                "Found Smoothness Texture: " + Smoothness.name
            );
        } else Debug.Log("Using Smoothness: " + Smoothness.name);

        //Occlusion
        if (Occlusion == null) {
            //Print status
            Debug.Log(
                Occlusion == null ?
                "No suitable Occlusion texture found" :
                "Found Occlusion Texture: " + Occlusion.name
            );
        } else Debug.Log("Using Occlusion: " + Occlusion.name);

        //Detail
        if (DetailMask == null) {
            //Print status
            Debug.Log(
                DetailMask == null ?
                "No suitable DetailMask texture found" :
                "Found DetailMask Texture: " + DetailMask.name
            );
        } else Debug.Log("Using DetailMask: " + DetailMask.name);*/
    }

    public void ExternAwake() {
        //Load persistent data
        //Keywords
        MetallicKeywords   = EditorPrefs.GetString(EDITOR_PREFS_PREFIX + "MetallicKeywords"  , "Metallic, Metalness, Metal");
        SmoothnessKeywords = EditorPrefs.GetString(EDITOR_PREFS_PREFIX + "SmoothnessKeywords", "Smoothness, Roughness");
        OcclusionKeywords  = EditorPrefs.GetString(EDITOR_PREFS_PREFIX + "OcclusionKeywords" , "Occlusion, AO");
        DetailMaskKeywords = EditorPrefs.GetString(EDITOR_PREFS_PREFIX + "DetailMaskKeywords", "Detail");

        //Metallic preferences
        MetallicAsSmoothness =  EditorPrefs.GetBool(EDITOR_PREFS_PREFIX + "MetallicAsSmoothness", false);
        InvertSmoothness = EditorPrefs.GetBool(EDITOR_PREFS_PREFIX + "InvertSmoothness", false);
        
        //Resolution preferences
        OutputFileResolution = new Vector2Int(
            EditorPrefs.GetInt(EDITOR_PREFS_PREFIX + "OutputFileResolutionX", 4096),
            EditorPrefs.GetInt(EDITOR_PREFS_PREFIX + "OutputFileResolutionY", 4096)
        );

        //Default Value when missing texture
        DefaultMetallicValue   = EditorPrefs.GetFloat(EDITOR_PREFS_PREFIX + "DefaultMetallicValue"  , 0f);
        DefaultSmoothnessValue = EditorPrefs.GetFloat(EDITOR_PREFS_PREFIX + "DefaultSmoothnessValue", 1f);
        DefaultOcclusionValue  = EditorPrefs.GetFloat(EDITOR_PREFS_PREFIX + "DefaultOcclusionValue" , 1f);
        DefaultDetailMaskValue = EditorPrefs.GetFloat(EDITOR_PREFS_PREFIX + "DefaultDetailMaskValue", 1f);


    }

    public void ExternOnDestroy() {
        //Write persistent data
        //Keywords
        EditorPrefs.SetString(EDITOR_PREFS_PREFIX + "MetallicKeywords"  , MetallicKeywords);
        EditorPrefs.SetString(EDITOR_PREFS_PREFIX + "SmoothnessKeywords", SmoothnessKeywords);
        EditorPrefs.SetString(EDITOR_PREFS_PREFIX + "OcclusionKeywords" , OcclusionKeywords);
        EditorPrefs.SetString(EDITOR_PREFS_PREFIX + "DetailMaskKeywords", DetailMaskKeywords);

        //Metallic preferences
        EditorPrefs.SetBool(EDITOR_PREFS_PREFIX + "MetallicAsSmoothness", MetallicAsSmoothness);
        EditorPrefs.SetBool(EDITOR_PREFS_PREFIX + "InvertSmoothness", InvertSmoothness);

        //Resolution preferences
        EditorPrefs.SetInt(EDITOR_PREFS_PREFIX + "OutputFileResolutionX", OutputFileResolution.x);
        EditorPrefs.SetInt(EDITOR_PREFS_PREFIX + "OutputFileResolutionY", OutputFileResolution.y);

        //Default Value when missing texture
        EditorPrefs.SetFloat(EDITOR_PREFS_PREFIX + "DefaultMetallicValue"  , DefaultMetallicValue);
        EditorPrefs.SetFloat(EDITOR_PREFS_PREFIX + "DefaultSmoothnessValue", DefaultSmoothnessValue);
        EditorPrefs.SetFloat(EDITOR_PREFS_PREFIX + "DefaultOcclusionValue" , DefaultOcclusionValue);
        EditorPrefs.SetFloat(EDITOR_PREFS_PREFIX + "DefaultDetailMaskValue", DefaultDetailMaskValue);
    }

    //[MenuItem("Window/Chan's Texture Tools/Clear Preferences for MaskMaker")]
    /*public void ResetPreferences()
    {
        //Write persistent data
        //Keywords
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "MetallicKeywords"  );
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "SmoothnessKeywords");
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "OcclusionKeywords" );
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "DetailMaskKeywords");

        //Metallic preferences
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "MetallicAsSmoothness");

        //Resolution preferences
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "OutputFileResolutionX");
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "OutputFileResolutionY");

        //Default Value when missing texture
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "DefaultMetallicValue"  );
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "DefaultSmoothnessValue");
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "DefaultOcclusionValue" );
        EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "DefaultDetailMaskValue");

        ExternAwake();
    }*/
}