﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using static TextureUtils;
public class ExtraTools : TextureTool
{
    //Extra Tools
    static readonly string EDITOR_PREFS_PREFIX = "TextureTool_Extras_";
    Texture2D NormalMap = null;

    public ExtraTools()
    {

    }

    public string GetName()
    {
        return "Extra Tools";
    }

    public string GetEditorPrefsPrefix()
    {
        return EDITOR_PREFS_PREFIX;
    }

    public void ExternOnGUI()
    {
        GUILayout.Label("Extra Tools", EditorStyles.boldLabel);
        NormalMap = (Texture2D) EditorGUILayout.ObjectField("NormalMap", NormalMap, typeof (Texture2D), false);
        if(GUILayout.Button(new GUIContent("Invert Green Channel (overwrites selected file)", "Used to convert between a DirectX normal and an OpenGL normal. They are completely identical except the green channel is inverted from one to another. Do not use this with a JPG unless you are sure it is a DirectX texture as JPG loses quality each time it is written. Uses JPG quality 100.")))
        {   
            if (NormalMap == null)
                goto CANCEL_NORMAL_INVERT;

            InvertNormal();
        }
        CANCEL_NORMAL_INVERT:

        EditorGUILayout.Space();
    }

    //Function to invert the normal's Green value and overwrite the original file
    void InvertNormal()
    {
        Texture2D RawNormal = TextureUtils.GetTextureFromDisk(NormalMap);

            int width  = RawNormal.width;
            int height = RawNormal.height;

            for (int x = 0; x < width; x++)
		    {
			    for (int y = 0; y < height; y++)
			    {
                    RawNormal.SetPixel(
                        x,
                        y,
                        new Color(
                                 RawNormal.GetPixel(x, y).r,
                            1f - RawNormal.GetPixel(x, y).g, //Invert green, that's all folks  
                                 RawNormal.GetPixel(x, y).b
                        )
                    );
                }
            }

            //Decide where it goes (overwrite original)
            string texturePath = AssetDatabase.GetAssetPath(NormalMap);

            //Determine encoding type and write to disk
            switch(new FileInfo(texturePath).Extension.ToLower())
            {
                case ".png":
                    File.WriteAllBytes(texturePath, RawNormal.EncodeToPNG());
                    Debug.Log("Wrote file \"" + texturePath + "\" using PNG encoding");
                    break;
                case ".jpg":
                    File.WriteAllBytes(texturePath, RawNormal.EncodeToJPG(100));
                    Debug.Log("Wrote file \"" + texturePath + "\" using JPG encoding");
                    break;
                case ".tga":
                    File.WriteAllBytes(texturePath, RawNormal.EncodeToTGA());
                    Debug.Log("Wrote file \"" + texturePath + "\" using TGA encoding");
                    break;
                case ".exr":
                    File.WriteAllBytes(texturePath, RawNormal.EncodeToEXR(Texture2D.EXRFlags.CompressZIP));
                    Debug.Log("Wrote file \"" + texturePath + "\" using EXR encoding");
                    break;
                default:
                File.WriteAllBytes(texturePath, RawNormal.EncodeToPNG());
                    Debug.Log("Wrote file \"" + texturePath + "\" using PNG encoding");
                    break;
            }

            //Inform user of success

            //Import the new file
            AssetDatabase.ImportAsset(TextureUtils.FullNameToUnityName(texturePath));
    }

    public void ExternAwake() {
        
    }

    public void ExternOnDestroy() {
        
    }

}
