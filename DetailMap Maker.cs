﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using static TextureUtils;

//https://docs.unity3d.com/Packages/com.unity.render-pipelines.high-definition@7.1/manual/Mask-Map-and-Detail-Map.html
public class DetailMapMaker : TextureTool
{
    static readonly string EDITOR_PREFS_PREFIX = "Detail_Map_Maker_";

    Texture2D Albedo;
    Texture2D NormalX;
    Texture2D NormalY;
    Texture2D Smoothness;
    bool OneNormal = true;
    bool UseSmoothnessAlpha = true;
    bool InvertSmoothness = false;

    float DefaultAlbedoValue = 1f;
    float DefaultNormalXValue = 0.5f;
    float DefaultNormalYValue = 0.5f;
    float DefaultSmoothnessValue = 0f;

    bool ShowKeywords = false;
    string AlbedoKeywords = "Albedo, Color";
    string NormalXKeywords = "Normal, Norm";
    string NormalYKeywords = "Normal, Norm";
    string SmoothnessKeywords = "Smoothness, Roughness";

    string OutputFileName = "DetailMap";
    Vector2Int OutputFileResolution = new Vector2Int(4096, 4096);
    

    
    public string GetName()
    {
        return "DetailMap Maker";
    }
    public string GetEditorPrefsPrefix()
    {
        return EDITOR_PREFS_PREFIX;
    }
    public void ExternOnGUI()
    {
        /*
        True Operation
        Albedo (Grayscale) -> R
        Normal (Green, Y)  -> G
        Smoothness (Gray)  -> B
        Normal (Red, X)    -> A
        */

        GUILayout.Label("Input Textures", EditorStyles.boldLabel);

        if(GUILayout.Button("Clear Textures"))
        {
            Albedo = null;
            NormalX = null;
            NormalY = null;
            Smoothness = null;
        }
        if (GUILayout.Button("Reset Sliders"))
        {
            DefaultAlbedoValue   = 0f;
            DefaultNormalXValue  = .5f;
            DefaultNormalYValue = .5f;
            DefaultSmoothnessValue = 0f;
        }
        EditorGUILayout.Space();



        EditorGUILayout.Space();
        //Albedo Map Input
        Albedo = (Texture2D) EditorGUILayout.ObjectField("Albedo (R)", Albedo, typeof (Texture2D), false); 
        //Albedo Default
        if (Albedo == null)
            DefaultAlbedoValue = EditorGUILayout.Slider(new GUIContent("", "Value to use for the above channel in the absence of a texture"), DefaultAlbedoValue, 0f, 1f);
        else
        {
            GUI.enabled = false;
            GUILayout.Label("Using Grayscale RGB from " + Albedo.name);
            GUI.enabled = true;
        }

        //Normal X or Mono
        EditorGUILayout.Space();
        string[] normalOptions = {"One Normal", "Split X and Y Normals"};
        OneNormal = EditorGUILayout.Popup("Normal Mode", OneNormal ? 0 : 1, normalOptions) == 0; 
        //NormalX Map Input, treated as the only normal if set to do so
        NormalX = (Texture2D) EditorGUILayout.ObjectField(OneNormal ? "Normal (A, G)" : "Normal X (A)", NormalX, typeof (Texture2D), false); 
        //NormalX Default
        if (NormalX == null)
            DefaultNormalXValue = EditorGUILayout.Slider(new GUIContent("", "Value to use for the above channel in the absence of a texture"), DefaultNormalXValue, 0f, 1f);
        else
        {
            GUI.enabled = false;
            GUILayout.Label("Using " + (OneNormal ? "R and G channels" : "R channel") + " from " + NormalX.name);
            GUI.enabled = true;
        }

        //Normal Y
        if (!OneNormal){
            EditorGUILayout.Space();
            //NormalY Map Input
            NormalY = (Texture2D) EditorGUILayout.ObjectField("Normal Y (G)", NormalY, typeof (Texture2D), false); 
            //NormalY Default
            if (NormalY == null)
                DefaultNormalYValue = EditorGUILayout.Slider(new GUIContent("", "Value to use for the above channel in the absence of a texture"), DefaultNormalYValue, 0f, 1f);
            else
            {
                GUI.enabled = false;
                GUILayout.Label("Using G channel from " + NormalY.name);
                GUI.enabled = true;
            }
        }
        else
        {
            //Single normal mode
            NormalY = NormalX;
        }

        EditorGUILayout.Space();
        //Smoothness Map Input
        Smoothness = (Texture2D) EditorGUILayout.ObjectField("Smoothness (B)", Smoothness, typeof (Texture2D), false); 
        //Smoothness Default
        if (Smoothness == null)
            DefaultSmoothnessValue = EditorGUILayout.Slider(new GUIContent("", "Value to use for the above channel in the absence of a texture"), DefaultSmoothnessValue, 0f, 1f);
        else
        {
            GUI.enabled = false;
            GUILayout.Label("Using " + (UseSmoothnessAlpha ? "A" : "R") + " channel from " + Smoothness.name);
            GUI.enabled = true;
            InvertSmoothness = EditorGUILayout.Toggle("Invert Smoothness", InvertSmoothness, GUILayout.ExpandWidth(false));
        }

        string[] smoothnessOptions = {"Use smoothness alpha channel", "Use smoothness red or gray channel"};
        UseSmoothnessAlpha = EditorGUILayout.Popup("Smoothness Mode", UseSmoothnessAlpha ? 0 : 1, smoothnessOptions) == 0; 


        //--------------------------------------------------------------------------------------------------------------------------

        //Button to auto-detect remaining maps using Keywords
        if(GUILayout.Button(
            new GUIContent("Auto-detect other textures", "This button takes the first (top to bottom) defined texture, opens its directory, and searches other files in the directory for keywords that define other textures")
        ))
        {
            if (!HasNonNullTexture())
            {
                //No texture found, cancel
                Debug.LogWarning("Select at least one texture before attempting to run auto-detect");
                goto CANCEL_AUTODETECT;
            }

            AutoDetectTextures();
        }
        CANCEL_AUTODETECT:



        ShowKeywords = EditorGUILayout.Foldout(ShowKeywords, "Keywords for auto-detection");
        
        if (ShowKeywords)
        {
            GUILayout.Label("Case sensitive, separate with commas, hover over button above for details");
            AlbedoKeywords     = EditorGUILayout.TextField ("Albedo", AlbedoKeywords);
            NormalXKeywords    = EditorGUILayout.TextField ("Normal X", NormalXKeywords);
            NormalYKeywords    = EditorGUILayout.TextField ("Normal Y", NormalYKeywords);
            SmoothnessKeywords = EditorGUILayout.TextField ("Smoothness", SmoothnessKeywords);
            if (GUILayout.Button("Reset Keywords"))
            {
                //Delete Keywords
                EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "AlbedoKeywords"); 
                EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "NormalXKeywords"); 
                EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "NormalYKeywords"); 
                EditorPrefs.DeleteKey(EDITOR_PREFS_PREFIX + "SmoothnessKeywords"); 
                ExternAwake();
            }
            EditorGUILayout.Space();
        }


        //--------------------------------------------------------------------------------------------------------------------------


        //Label for Output Options
        EditorGUILayout.Space();
        GUILayout.Label("Output Options", EditorStyles.boldLabel);
        
        //Used to pick the output file name
        OutputFileName = EditorGUILayout.TextField("Output File Name", OutputFileName);
        
        //Begin horizontal resolution picker
        GUILayout.BeginHorizontal();
        GUILayout.Label("Output Resolution");
        OutputFileResolution = EditorGUILayout.Vector2IntField("", OutputFileResolution);
        EditorGUILayout.EndHorizontal();

        if(GUILayout.Button(new GUIContent("Auto-set resoltion", "Reads the original texture asset fromt he disk and sets the output resolution equal to the input texture with the largest area")))
        {
            if (!HasNonNullTexture())
            {
                Debug.LogWarning("Select at least one texture before attempting to auto-set resolution");
                goto CANCEL_AUTORES;
            }

            OutputFileResolution = AutoResolution(Albedo, NormalX, NormalY, Smoothness);
        }
        CANCEL_AUTORES:

        //Render button
        if(GUILayout.Button("Render and Save DetailMap"))
        {
            RenderMap();
        }
    }

    public void RenderMap()
    {
        long renderStartTime = System.DateTime.Now.Ticks;

        //Texture to render to
        Texture2D OutputTexture = new Texture2D(OutputFileResolution.x, OutputFileResolution.y);
        
        //Render
        int width  = OutputTexture.width;
        int height = OutputTexture.height;
        int length = width * height;

        //Get pixel data straight from the disk
        EditorUtility.DisplayProgressBar("Rendering...", "Reading and preparing textures", 0f);
        Color[] AlbedPixels = TextureUtils.GetPixelDataFromDisk(OutputFileResolution, DefaultAlbedoValue    , Albedo);
        EditorUtility.DisplayProgressBar("Rendering...", "Reading and preparing textures", 0.25f);
        Color[] NormXPixels = TextureUtils.GetPixelDataFromDisk(OutputFileResolution, DefaultNormalXValue   , NormalX);
        EditorUtility.DisplayProgressBar("Rendering...", "Reading and preparing textures", 0.50f);
        Color[] NormYPixels = TextureUtils.GetPixelDataFromDisk(OutputFileResolution, DefaultNormalYValue   , NormalY);
        EditorUtility.DisplayProgressBar("Rendering...", "Reading and preparing textures", 0.75f);
        Color[] SmootPixels = TextureUtils.GetPixelDataFromDisk(OutputFileResolution, DefaultSmoothnessValue, Smoothness);

        Color[] OutPixels = new Color[length];

        //Used so the progress bar only updates 100 times
        int lengthDiv100 = length / 100;

        //Render
        /*
        True Operation
        Albedo (Grayscale) -> R
        Normal (Green, Y)  -> G
        Smoothness (Gray)  -> B
        Normal (Red, X)    -> A

        User Informed
        Albedo (Grayscale) -> R
        X RX A
        Y G G
        Smoothness A B
        */
        for (int i = 0; i < length; i++)
        {
            //Use Albedo Grayscale for Red
            OutPixels[i].r = TextureUtils.ToGrayscaleFloat(AlbedPixels[i]);
            //Use Normal Green/Y for Green
            OutPixels[i].g = NormYPixels[i].g;
            //Use Smoothness Alpha-or-Red for Blue
            OutPixels[i].b = UseSmoothnessAlpha ?
                SmootPixels[i].a : //Use Alpha from Smoothness
                SmootPixels[i].r;  //Use Red   from Smoothness

            if (InvertSmoothness)
                OutPixels[i].b = 1f - OutPixels[i].b;

            //Use Normal Red/X for Alpha
            OutPixels[i].a = NormXPixels[i].r;

            //Output 100 times
            if (i % lengthDiv100 == 0)
                EditorUtility.DisplayProgressBar("Rendering...", "Rendering (" + i + "/" + length + ")", (float) i / (float) length);
        }

        //Debug
        EditorUtility.DisplayProgressBar("Rendering...", "Writing texture data", 1f);

        //Apply the modified pixels to the image
        OutputTexture.SetPixels(OutPixels);
        OutputTexture.Apply();

        //Decide where it goes
        DirectoryInfo outputDiectory = GetDirectoryFromFirstNonNullTexture();
        FileInfo outputFile = new FileInfo(outputDiectory.FullName + "/" + OutputFileName + ".png");

        //Actually write it to the disk
        File.WriteAllBytes(outputFile.FullName, OutputTexture.EncodeToPNG());

        //Inform user of success EDIT: This is done later, not needed
        //Debug.Log("Wrote file \"" + outputFile.FullName + "\"");

        //Import the new file
        AssetDatabase.ImportAsset(TextureUtils.FullNameToUnityName(outputFile.FullName));
        
        //Remove progress bar
        EditorUtility.ClearProgressBar();
        
        //Display elapsed time
        string timeText = ElapsedTimeTextFromTicks(renderStartTime);
        Debug.Log("Render Complete, Elapsed = " + timeText + "; Written to \"" + outputFile.FullName + "\"");

    }

    private void AutoDetectTextures()
    {
        DirectoryInfo textureDir = GetDirectoryFromFirstNonNullTexture();

        //Replace null textures with the detected texture

        //Albedo
        if (Albedo == null) {
            //First get the file
            FileInfo DetectedAlbedoFile = fileWithKeyword(textureDir, AlbedoKeywords.Split(','));
            //Load it
            Albedo = DetectedAlbedoFile == null ? null : AssetDatabase.LoadAssetAtPath<Texture2D>(TextureUtils.FullNameToUnityName(DetectedAlbedoFile.FullName));
            //Print status
            Debug.Log(
                Albedo == null ?
                "No suitable Albedo texture found" :
                "Found Albedo Texture: " + Albedo.name
            );
        } else Debug.Log("Using Existing Albedo: " + Albedo.name);

        //NormalX
        if (NormalX == null) {
            //First get the file
            FileInfo DetectedNormalXFile = fileWithKeyword(textureDir, NormalXKeywords.Split(','));
            //Load it
            NormalX = DetectedNormalXFile == null ? null : AssetDatabase.LoadAssetAtPath<Texture2D>(TextureUtils.FullNameToUnityName(DetectedNormalXFile.FullName));
            //Print status
            Debug.Log(
                NormalX == null ?
                "No suitable NormalX texture found" :
                "Found NormalX Texture: " + NormalX.name
            );
        } else Debug.Log("Using Existing NormalX: " + NormalX.name);

        //NormalY
        if (NormalY == null) {
            //First get the file
            FileInfo DetectedNormalYFile = fileWithKeyword(textureDir, NormalYKeywords.Split(','));
            //Load it
            NormalY = DetectedNormalYFile == null ? null : AssetDatabase.LoadAssetAtPath<Texture2D>(TextureUtils.FullNameToUnityName(DetectedNormalYFile.FullName));
            //Print status
            Debug.Log(
                NormalY == null ?
                "No suitable NormalY texture found" :
                "Found NormalY Texture: " + NormalY.name
            );
        } else Debug.Log("Using Existing NormalY: " + NormalY.name);

        //Detail
        if (Smoothness == null) {
            //First get the file
            FileInfo DetectedSmoothnessFile = fileWithKeyword(textureDir, SmoothnessKeywords.Split(','));
            //Load it
            Smoothness = DetectedSmoothnessFile == null ? null : AssetDatabase.LoadAssetAtPath<Texture2D>(TextureUtils.FullNameToUnityName(DetectedSmoothnessFile.FullName));
            //Print status
            Debug.Log(
                Smoothness == null ?
                "No suitable Smoothness texture found" :
                "Found Smoothness Texture: " + Smoothness.name
            );
        } else Debug.Log("Using Existing Smoothness: " + Smoothness.name);
    }

    public void ExternAwake() {
        //Load persistent data
        //Keywords
        AlbedoKeywords     = EditorPrefs.GetString(EDITOR_PREFS_PREFIX + "AlbedoKeywords"    , "Albedo, Color");
        NormalXKeywords    = EditorPrefs.GetString(EDITOR_PREFS_PREFIX + "NormalXKeywords"   , "Normal, Norm");
        NormalYKeywords    = EditorPrefs.GetString(EDITOR_PREFS_PREFIX + "NormalYKeywords"   , "Normal, Norm");
        SmoothnessKeywords = EditorPrefs.GetString(EDITOR_PREFS_PREFIX + "SmoothnessKeywords", "Smoothness, Roughness");

        //Normal Mode
        OneNormal =        EditorPrefs.GetBool(EDITOR_PREFS_PREFIX + "OneNormal", false);
        InvertSmoothness = EditorPrefs.GetBool(EDITOR_PREFS_PREFIX + "InvertSmoothness", false);
        
        //Resolution preferences
        OutputFileResolution = new Vector2Int(
            EditorPrefs.GetInt(EDITOR_PREFS_PREFIX + "OutputFileResolutionX", 4096),
            EditorPrefs.GetInt(EDITOR_PREFS_PREFIX + "OutputFileResolutionY", 4096)
        );

        //Default Value when missing texture
        DefaultAlbedoValue     = EditorPrefs.GetFloat(EDITOR_PREFS_PREFIX + "DefaultAlbedoValue"  , 0f);
        DefaultNormalXValue    = EditorPrefs.GetFloat(EDITOR_PREFS_PREFIX + "DefaultNormalXValue" , 1f);
        DefaultNormalYValue    = EditorPrefs.GetFloat(EDITOR_PREFS_PREFIX + "DefaultNormalYValue", 1f);
        DefaultSmoothnessValue = EditorPrefs.GetFloat(EDITOR_PREFS_PREFIX + "DefaultSmoothnessValue", 1f);


    }

    public void ExternOnDestroy() {
        //Write persistent data
        //Keywords
        EditorPrefs.SetString(EDITOR_PREFS_PREFIX + "AlbedoKeywords"    , AlbedoKeywords);
        EditorPrefs.SetString(EDITOR_PREFS_PREFIX + "NormalXKeywords"   , NormalXKeywords);
        EditorPrefs.SetString(EDITOR_PREFS_PREFIX + "NormalYKeywords"   , NormalYKeywords);
        EditorPrefs.SetString(EDITOR_PREFS_PREFIX + "SmoothnessKeywords", SmoothnessKeywords);

        //Normal preferences
        EditorPrefs.SetBool(EDITOR_PREFS_PREFIX + "OneNormal", OneNormal);
        EditorPrefs.SetBool(EDITOR_PREFS_PREFIX + "InvertSmoothness", InvertSmoothness);

        //Resolution preferences
        EditorPrefs.SetInt(EDITOR_PREFS_PREFIX + "OutputFileResolutionX", OutputFileResolution.x);
        EditorPrefs.SetInt(EDITOR_PREFS_PREFIX + "OutputFileResolutionY", OutputFileResolution.y);

        //Default Value when missing texture
        EditorPrefs.SetFloat(EDITOR_PREFS_PREFIX + "DefaultAlbedoValue"    , DefaultAlbedoValue);
        EditorPrefs.SetFloat(EDITOR_PREFS_PREFIX + "DefaultNormalXValue"   , DefaultNormalXValue);
        EditorPrefs.SetFloat(EDITOR_PREFS_PREFIX + "DefaultNormalYValue"   , DefaultNormalYValue);
        EditorPrefs.SetFloat(EDITOR_PREFS_PREFIX + "DefaultSmoothnessValue", DefaultSmoothnessValue);
    }

        private static FileInfo fileWithKeyword (DirectoryInfo directory, string[] keywords)
    {
        //Iterate through all files in the directory
        foreach (FileInfo file in directory.GetFiles())
        {
            //Check if that file contains a keyword
            if (file.Extension != ".meta" && containsKeyword(file.Name, keywords))
                return file;
        }

        return null;
    }

    //Checks if the first argument contains any of the entries in the second argument
    private static bool containsKeyword (string searchThis, string[] keywords)
	{
		foreach (string keyword in keywords)
		{
			if (searchThis.Contains(keyword.Trim()))
				return true;
		}
		
		return false;
	}

    private bool HasNonNullTexture()
    {
        return Albedo     != null
            || NormalX    != null
            || NormalY    != null
            || Smoothness != null;
    }

    private Texture2D GetFirstNonNullTexture()
    {
        //Search for the first non-null texture
        Texture2D nonNullTexture = null;
        if (nonNullTexture == null && Albedo     != null) nonNullTexture = Albedo;
        if (nonNullTexture == null && NormalX    != null) nonNullTexture = NormalX;
        if (nonNullTexture == null && NormalY    != null) nonNullTexture = NormalY;
        if (nonNullTexture == null && Smoothness != null) nonNullTexture = Smoothness;

        return nonNullTexture;
    }

    private DirectoryInfo GetDirectoryFromFirstNonNullTexture()
    {
        //Path of first non-null texture
        string texturePath = AssetDatabase.GetAssetPath(GetFirstNonNullTexture());
        //Directory containing textures
        return new FileInfo(texturePath).Directory;
    }
    
}
