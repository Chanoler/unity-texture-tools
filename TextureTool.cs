﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface TextureTool
{
    string GetName();
    void ExternOnGUI();
    void ExternAwake();
    void ExternOnDestroy();
    string GetEditorPrefsPrefix();
}
